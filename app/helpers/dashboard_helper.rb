module DashboardHelper

  def show_delta(value)
    if value > 0
      return "<img src='/assets/green_delta.png'> #{value.abs.to_s} %".html_safe 
    elsif value == 0
      return "<img src='/assets/gray_circle.gif'> #{value.abs.to_s} %".html_safe 
    else
      return "<img src='/assets/red_delta.png'> #{value.abs.to_s} %".html_safe 
    end
  end
  
  def name_size(muze_name)
    size = muze_name.length
    if size < 15
      return "large_name"
    else
      return "small_name"
    end
  end

  def count_color(value)
    if value > 0
      return "positive_count"
    elsif value == 0
      return "neutral_count"
    else
      return 'negative_count'
    end
  end
  
  def fb_hyper_link(muze)
    if muze.facebook.blank?
      return muze.full_name
    else
      return "<a href=#{muze.facebook} target='_blank'> #{muze.full_name} </a> ".html_safe
    end
  end
  
  def twitter_hyper_link(muze)
    if muze.twitter.blank?
      return muze.full_name
    else
      return "<a href=#{muze.twitter} target='_blank'> #{muze.full_name}</a>".html_safe
    end
  end
  
end