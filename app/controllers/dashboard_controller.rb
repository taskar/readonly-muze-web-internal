class DashboardController < ApplicationController

  before_filter :login_required

  MUZE_STATS_COLLECTION = "muze_stats"
  
  def index
    @stats = get_stats
    @muzes = get_muzes
    @followers_delta_array = get_followers_delta_array
    @likes_delta_array = get_likes_delta_array
  end
  
  def twitter_follower_counts
    # Get the last 7 days of stats
    render :json => get_stats
  end

  def facebook_likes_counts
    # Get the last 7 days of stats
    render :json => get_stats
  end

  private
  
  def fetch_last_two_dates
    dates = []
    $db['muze_stats'].find().sort(['run_at', "DESCENDING"]).limit(2).each do |doc|
      dates << doc['run_at']
    end
  return dates
  end
 
  #returns a hastable of muzes ids and their numbers of followers or likes for a given day. as follows:
  #muze_stats_by_id = {id=>twitter_followers, ..., id => twitter_followers}
  #along with the total number of followers for that day
  def get_muzes_follower_stats(day) 
     doc = $db['muze_stats'].find_one({'run_at' => day}) 
     muze_follower_stats_by_id = {}  

     doc['muzes'].each do |muze|
       id = muze.keys[0]
       muze_follower_stats_by_id[id] = muze[id]['twitter'].blank? ? 0 : muze[id]['twitter']['num_followers']
     end
     return muze_follower_stats_by_id, doc['total_twitter_followers']
  end

   def get_muzes_like_stats(day) 
     doc = $db['muze_stats'].find_one({'run_at' => day}) 
     muze_like_stats_by_id = {}  

     doc['muzes'].each do |muze|
       id = muze.keys[0]
       muze_like_stats_by_id[id] = muze[id]['facebook'].blank? ? 0 : muze[id]['facebook']['num_likes']
     end
     return muze_like_stats_by_id, doc['total_num_likes']
   end
   
  def get_followers_delta_array
    return @followers_delta_array if !@followers_delta_array.blank?
    day2, day1 = fetch_last_two_dates
    muze_stats_day1, tot_followers_day1 = get_muzes_follower_stats(day1)
    muze_stats_day2, tot_followers_day2 = get_muzes_follower_stats(day2)
    followers_delta_array = Muze_stats.delta_array_generator(muze_stats_day1, muze_stats_day2, tot_followers_day1, tot_followers_day2)
    return followers_delta_array 
  end
  
  def get_likes_delta_array
    return @likes_delta_array if !@likes_delta_array.blank? 
    day2, day1 = fetch_last_two_dates
    muze_stats_day1, tot_day1 = get_muzes_like_stats(day1)
    muze_stats_day2, tot_day2 = get_muzes_like_stats(day2)
    likes_delta_array = Muze_stats.delta_array_generator(muze_stats_day1, muze_stats_day2, tot_day1, tot_day2)
    return likes_delta_array
  end
  
  # Retrieve the stats for the last 7 days
  def get_stats
    return @stats if !@stats.blank?
    @stats = []
    $db['muze_stats'].find().sort(['run_at', "DESCENDING"]).each do |doc|
      @stats.unshift({"run_at" => doc['run_at'],
                      "total_twitter_followers" => doc['total_twitter_followers'],
                      "total_num_likes" => doc['total_num_likes']})
    end
    @stats
  end

  def get_muzes
    return @most_recent_stats if !@most_recent_stats.blank?
    @most_recent_stats = []
    $db['muze_stats'].find().sort(['run_at', "DESCENDING"]).limit(1).each do |doc|
      doc['muzes'].each do |hash|
        muze_id = hash.keys.first
        data = hash.values.first
        muze = Muzes.find_by_id(muze_id)
        next if muze.blank?
        muze.num_twitter_followers = data['twitter'].blank? ? 0 : data['twitter']['num_followers']   
        muze.num_likes = data['facebook'].blank? ? 0 : data['facebook']['num_likes']
        @most_recent_stats << muze
      end 
    end
    @most_recent_stats
  end

end
