class MuzesController < ApplicationController

  def index
    @muzes = Muzes.find_all({})
  end

  def show
    @muze = Muzes.find_by_id(params[:id])
  end

end
