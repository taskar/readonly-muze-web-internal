class Causes < MongoRecord

  # COLLECTION = "causes"

  attr_accessor :description

  def initialize(options)
    options.each {|k, v|  self.instance_variable_set("@#{k.to_s}", v) }
  end

  # # CLASS METHODS
  # def self.create(options)
  #   doc_id = $db[COLLECTION].insert(options)
  #   find_one('_id' => doc_id)
  # end

  # def self.find_all(options)
  #   records = []
  #   $db[COLLECTION].find(options).each do |doc|
  #     records << self.new(doc)
  #   end
  #   records
  # end

  # def self.find_one(options)
  #   doc = $db[COLLECTION].find_one(options)
  #   return self.new(doc)
  # end
end