class Industries < MongoRecord

  attr_accessor :label, :name

  def initialize(options)
    options.each {|k, v|  self.instance_variable_set("@#{k.to_s}", v) }
  end

end