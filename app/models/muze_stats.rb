class Muze_stats < MongoRecord
  
  #given two numbers (number of followers or number of likes from two different days) 
  #this method returns the percent change in those values
  def self.calculate_percent_change(day1value, day2value)
	  if day1value == 0 and day2value == 0 
	    percent_diff = 0
	  elsif day1value == 0
	    percent_diff = 100
	  else
	    difference = day2value - day1value
	    percent_diff = (difference.to_f/day1value*100).round(2)
	  end  
	  return percent_diff
	end
		
	#returns a hashtable with muzes ids and percent change of either their followers and likes.
	#the hashtable also has key='all_muzes' for the total percent change for all muzes combined.
	# delta_array = {'all_muzes' => delta_%, id_1 => delta_%, ...,id_n => delta_%} 
  def self.delta_array_generator(muze_stats_day1, muze_stats_day2, tot_day1, tot_day2)       
    delta_array = {} 
    
    #muzes_breakdown: new, dropped, old 
    new_muzes_id = muze_stats_day2.keys - muze_stats_day1.keys
    dropped_muzes_id = muze_stats_day1.keys - muze_stats_day2.keys 
    old_muzes_id = muze_stats_day2.keys - new_muzes_id
    	
    #add all the new muzes
    new_muzes_id.each {|id| delta_array[id] = 100}
      
    #add all the dropped muzes
    dropped_muzes_id.each {|id| delta_array[id] = -100}
          	
    #take care of the remaining muzes 
    old_muzes_id.each {|id| delta_array[id] = calculate_percent_change(muze_stats_day1[id], muze_stats_day2[id])}
      
	  #calculate the total percent difference
    delta_array['all_muzes'] = calculate_percent_change(tot_day1, tot_day2)

	  return delta_array
	end
	
end