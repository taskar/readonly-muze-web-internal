class Muzes < MongoRecord

  attr_reader :id, :bson_id

  attr_accessor :content, :title, :slug, :pullquote, :occupation,
                :twitter, :facebook, :instagram, :pinterest, :youtube,
                :num_twitter_followers, :num_likes, :firstName, :lastName

  def initialize(options)
    options.each {|k, v|  self.instance_variable_set("@#{k.to_s}", v) }
  end

  def id
    @_id.to_s
  end

  def thumbnail_image
    @attachments.first['images']['thumbnail']['url']
  end

  def profile_image
    @attachments.first['images']['profile-image']['url']
  end

  def get_url
    "/muzes/show/#{self.id}"
  end

  def get_causes
    if !@causes.blank?
      return Causes.find_all({'wp_id' => {'$in' => @causes}})
    end
  end

  def get_industries
    if !@industries.blank?
      Industries.find_all({'wp_id' => {'$in' => @industries}})
    end
  end
  
  def full_name
    return firstName+ " " +lastName
  end

end