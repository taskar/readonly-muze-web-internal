class Content < MongoRecord

  attr_accessor :title, :title_plain, :content, :excerpt

  def initialize(options)
    options.each {|k, v|  self.instance_variable_set("@#{k.to_s}", v) }
  end

  def thumb(proportion = nil)
    version = proportion.nil? ? "thumbnail" : "#{proportion}-thumb"
    @attachments.first['images'][version]['url']
  end

end