# Parent class for all models that persist in MongoDB
class MongoRecord

  # CLASS METHODS
  def self.create(options)
    doc_id = $db[collection_name].insert(options)
    find_one('_id' => doc_id)
  end

  def self.find_all(options)
    records = []
    $db[collection_name].find(options).each do |doc|
      records << self.new(doc)
    end
    records
  end

  def self.find_one(options)
    doc = $db[collection_name].find_one(options)
    return self.new(doc) if doc
  end
  
  def self.find_by_id(id)
    find_one({'_id' => BSON::ObjectId.from_string(id)})
  end

  def self.collection_name
    self.to_s.downcase
  end

end