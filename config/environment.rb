# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Muze::Application.initialize!

# MongoDb Configuration
@@mongo_config = YAML.load(File.read('config/database.yml'))["mongo_#{Rails.env}"]
conn = Mongo::Connection.new(@@mongo_config['host'], @@mongo_config['port'])
$db = conn.db(@@mongo_config['database'])
$db.authenticate(@@mongo_config['username'], @@mongo_config['password'])

require 'background_jobs'