require 'spec_helper'

describe Muze_stats do
  it "should return 0 (muze doesn't have followers)" do
    Muze_stats.calculate_percent_change(0, 0).should eq(0)
  end
  
  it "should return 100 (new account, didn't have any followers on previous day)" do
    Muze_stats.calculate_percent_change(0, 12312).should eq(100)
  end
  
  it "should return a positive integer (increase number of followers/likes)" do 
    percent = Muze_stats.calculate_percent_change(12, 321)
    percent.should > 0
  end
  
  it "should return a negative integer (decrease number of followers/likes)" do 
    percent = Muze_stats.calculate_percent_change(412, 321)
    percent.should < 0
  end
  
  it "should return 0 (same number of followers/likes)" do 
    percent = Muze_stats.calculate_percent_change(321, 321)
    percent.should eq(0)
  end
  
  
  
  
end