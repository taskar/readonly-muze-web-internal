require 'spec_helper'

describe CampaignsController do

  after(:each) do
    clear_databases
  end

  it "should create a user to attribute actions to and set a cookie" do
    get :show, {:id => 'haiti_campaign_id'}
    cookies[:user_id].should_not be_nil
    assigns[:user].should_not be_nil
  end

  it "should find a user from a previous session using their cookie" do
    user = User.create({:created_at => Time.now.utc})
    request.cookies[:user_id] = user.id
    get :show, {:id => 'haiti_campaign_id'}
    assigns[:user].id.should == user.id
  end

end
