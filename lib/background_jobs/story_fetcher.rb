require 'content'
require 'json'
require 'net/http'

class StoryFetcher

  BASE_URL = "http://muzes.org/stories/"
  TAG_SLUG = "st"

  def perform()
    # fetch all the muzes using the Wordpress JSON API 
    json = http_fetch(BASE_URL + "?json=get_tag_posts&tag_slug=#{TAG_SLUG}&include=post_id")
    story_ids = json['posts'].map {|h| h['id'] }
  
    # store all muzes in mongodb
    story_ids.each do |id|
      # fetch the story data
      story_data = http_fetch(BASE_URL + "?json=get_post&post_id=#{id}")['post']
      story_data['wp_content_id'] = story_data['id']
      story_data['content_type'] = 'text'
      story_data.delete('id')

      # fetch meta data (link to muzes)
      meta_data = http_fetch(BASE_URL + "?json=muzes.get_meta&id=#{id}&key=_muze_link")
      muze_ids = meta_data['result'].map(&:to_i)
      story_data.merge!({"muzes" => muze_ids})

      # fetch the standfirst for the story
      standfirst_data = http_fetch(BASE_URL + "?json=muzes.get_meta&id=#{id}&key=_muze_standfirst")
      standfirst = standfirst_data['result'].first
      story_data.merge!({"standfirst" => standfirst})

      # fetch industries associated with story
      industries_causes = http_fetch(BASE_URL + "?json=muzes.get_categories&id=#{id}")
      industries = industries_causes['industries'].map {|ind| ind['term_id'].to_i } if !industries_causes['industries'].blank?
      causes = industries_causes['causes'].map {|cas| cas['term_id'].to_i } if !industries_causes['causes'].blank?
      story_data.merge!({"industries" => industries, "causes" => causes})

      # store content in mongo
      $db[Content.collection_name].update({'wp_content_id' => story_data['wp_content_id']}, {'$set' => story_data}, {:upsert => true})
    end
  end

  def http_fetch(url)
    uri = URI.parse(url)
    http_req = Net::HTTP::Get.new(uri.request_uri)
    response = Net::HTTP.start(uri.host, uri.port) {|http| http.request(http_req) }
    JSON.parse(response.body)
  end

end