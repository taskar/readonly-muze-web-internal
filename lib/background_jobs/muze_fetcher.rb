require 'json'
require 'muzes'
require 'net/http'

class MuzeFetcher

  BASE_URL = "http://muzes.org/muzes/"
  TAG_SLUG = "mz"

  def perform()
    # fetch all the muzes using the Wordpress JSON API 
    json = http_fetch(BASE_URL + "?json=get_tag_posts&tag_slug=#{TAG_SLUG}&include=post_id")
    muze_ids = json['posts'].map {|h| h['id'] }
  
    muze_ids.each do |id|
      # fetch basic data
      muze_data = http_fetch(BASE_URL + "?json=get_post&post_id=#{id}")['post']
      muze_data['wp_muze_id'] = muze_data['id']
      muze_data.delete('id')

      # fetch muze meta data
      muze_meta_data = http_fetch(BASE_URL  + "?json=muzes.get_muze_meta&id=#{id}")['result']

      muze_data.merge!(muze_meta_data)

      # fetch industries associated with story
      industries_causes = http_fetch(BASE_URL + "?json=muzes.get_categories&id=#{id}")
      industries = industries_causes['industries'].map {|ind| ind['term_id'].to_i } if !industries_causes['industries'].blank?
      causes = industries_causes['causes'].map {|cas| cas['term_id'].to_i } if !industries_causes['causes'].blank?
      muze_data.merge!({"industries" => industries, "causes" => causes})

      # store in mongodb
      $db[Muzes.collection_name].update({'wp_muze_id' => muze_data['wp_muze_id']}, {'$set' => muze_data}, {:upsert => true})
    end
  end

  def http_fetch(url)
    uri = URI.parse(url)
    http_req = Net::HTTP::Get.new(uri.request_uri)
    response = Net::HTTP.start(uri.host, uri.port) {|http| http.request(http_req) }
    JSON.parse(response.body)
  end


end