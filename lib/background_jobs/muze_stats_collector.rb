require 'json'

class MuzeStatsCollector

  TWITTER_CONSUMER_KEY = "LYN38dvijFtzba9v3qew"
  TWITTER_SECRET = "M84OSnM3nF6Z9HLEG98oM1WqzYZYrygpuiwb3JTOEQ"
  TWITTER_OAUTH_TOKEN = "16640997-cFeAJ5do00gaL71mRE3x1RYt4TwfXKo1G8Vk31jKk"
  TWITTER_OAUTH_SECRET = "P0Otv5EmVxdl4cwGHceJb3I09mbzdFw3gRfasLJHg"

  attr_accessor :twitter_client

  def initialize()
    @twitter_client = get_twitter_client()
  end

  def perform()

    stats_data = Hash.new()

    stats_data[:total_twitter_followers] = 0
    stats_data[:total_num_likes] = 0
    stats_data[:muzes] = []
    stats_data[:run_at] = Date.today.to_s

    # for each muze, get the number of twitter followers
    Muzes.find_all({}).each do |muze|
      muze_data = {}

      # get data from twitter
      twitter_info = get_twitter_info(muze)
      if !twitter_info.blank?
        stats_data[:total_twitter_followers] += twitter_info[:num_followers]
        muze_data[:twitter] = twitter_info
      end

      # get data from facebook
      facebook_info = get_facebook_info(muze)
      if !facebook_info.blank?
        stats_data[:total_num_likes] += facebook_info[:num_likes]
        muze_data[:facebook] = facebook_info
      end

      # merge in all data for muze
      stats_data[:muzes] << {muze.id => muze_data}

      # pause for 2 seconds so we dont piss off twitter
      sleep(2)

    end

    # store the stats for this run
    $db['muze_stats'].update({'run_at' => stats_data[:run_at]}, stats_data, {:upsert => true})

  end

  def get_twitter_client
    consumer = OAuth::Consumer.new(TWITTER_CONSUMER_KEY, TWITTER_SECRET, {:site => "http://api.twitter.com"})
    accessor = OAuth::AccessToken.new(consumer, TWITTER_OAUTH_TOKEN, TWITTER_OAUTH_SECRET)
    accessor
  end

  def get_twitter_info(muze)
    begin
      return {} if muze.twitter.blank?

      # extract the twitter handler
      twitter_handle = muze.twitter.split('/').last

      # query the twitter api for the user
      response = @twitter_client.get("/1/users/show.json?screen_name=#{twitter_handle}")
      return {} if response.code != "200"
      twitter_data = JSON.parse(response.body)

      # store number of followers, number following, last tweet date
      twitter_info = {:num_followers => twitter_data['followers_count'],
                      :num_friends => twitter_data['friends_count']}
      if twitter_data['status']
        twitter_info[:last_tweet_date] = Time.parse(twitter_data['status']['created_at']).utc
      end

      return twitter_info
    rescue Exception => e
      puts "Exception: #{e.message} - #{muze.inspect}"
      return {}
    end
  end

  def get_facebook_info(muze)
    begin
      return {} if muze.facebook.blank?

      # extract the username or id
      query_id = ""
      if muze.facebook =~ /id=([\d]+)$/
        query_id = $1
      else 
        query_id = muze.facebook.split("/").last
      end
      query_id.strip!

      if !query_id.blank?
        uri = URI.parse("http://graph.facebook.com/#{query_id}")
        http_req = Net::HTTP::Get.new(uri.request_uri)
        response = Net::HTTP.start(uri.host, uri.port) {|http| http.request(http_req) }
        hash = JSON.parse(response.body)
        return {:num_likes => hash['likes']} if hash['likes']
      else
        return {}
      end
    rescue Exception => e
      puts "Exception: #{e.message} - #{muze.inspect}"
      return {}
    end
  end

end