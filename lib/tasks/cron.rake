task :cron => :environment do

  # Fetch new Muzes
  Delayed::Job.enqueue(MuzeFetcher.new())

  # Get new stats
  Delayed::Job.enqueue(MuzeStatsCollector.new())

end