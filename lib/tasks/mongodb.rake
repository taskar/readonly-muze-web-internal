
namespace :mongodb do

  INDUSTRIES = [{"label" => "adventure", "name" => "Adventure", "wp_id" => 1000},
                {"label" => "art-design", "name" => "Art & Design", "wp_id" => 1001},
                {"label" => "business", "name" => "Business", "wp_id" => 1002},
                {"label" => "culture", "name" => "Culture", "wp_id" => 1003},
                {"label" => "environment", "name" => "Environment", "wp_id" => 1004},
                {"label" => "fashion", "name" => "Fashion", "wp_id" => 1005},
                {"label" => "film-photography", "name" => "Film & Photography", "wp_id" => 1006},
                {"label" => "humanitarianism", "name" => "Humanitarianism", "wp_id" => 1007},
                {"label" => "music", "name" => "Music", "wp_id" => 1008},
                {"label" => "politics", "name" => "Politics", "wp_id" => 1009},
                {"label" => "science-technology", "name" => "Science & Technology", "wp_id" => 1010},
                {"label" => "sports", "name" => "Sports", "wp_id" => 1011}]

  CAUSES = [{"label" => "climate-change", "name" => "Climate Change", "wp_id" => 1012},
            {"label" => "community", "name" => "Community", "wp_id" => 1013},
            {"label" => "conservation", "name" => "Conservation", "wp_id" => 1014},
            {"label" => "development", "name" => "Development", "wp_id" => 1015},
            {"label" => "education", "name" => "Education", "wp_id" => 1016},
            {"label" => "food", "name" => "Food", "wp_id" => 1017},
            {"label" => "fresh-water", "name" => "Fresh Water", "wp_id" => 1018},
            {"label" => "health", "name" => "Health", "wp_id" => 1019},
            {"label" => "human-rights", "name" => "Human Rights", "wp_id" => 1020},
            {"label" => "oceans", "name" => "Oceans", "wp_id" => 1021},
            {"label" => "sustainability", "name" => "Sustainability", "wp_id" => 1022},
            {"label" => "waste", "name" => "Waste", "wp_id" => 1023}]


  desc 'Loading Industries into mongodb'
  task :create_industries => :environment do
    INDUSTRIES.each {|ind| Industries.create(ind) }
  end

  desc 'Loading Causes into mongodb'
  task :create_causes => :environment do
    CAUSES.each {|ind| Causes.create(ind) }
  end

end